#!/bin/bash

cd $BITBUCKET_CLONE_DIR

# This is the default value for projectId parameter
PROJECT_ID_VALUE=`cat <<END
{
    "value": "yufqzxkbiecj7jot",
    "type": "text",
    "name": "projectId",
    "key": "projectId"
}
END`

# This is the default value for clusterId parameter
DWH_CLUSTER_ID_VALUE=`cat <<END
{
    "value": "pu2ksm",
    "type": "text",
    "name": "dwhClusterId",
    "key": "dwhClusterId"
}
END`

# This is the default value for accountId parameter
ACCOUNT_ID_VALUE=`cat <<END
{
    "value": "B3mTX6pCTRBSIMRej4q0",
    "type": "text",
    "name": "accountId",
    "key": "accountId"
}
END`

# This is the default value for access token
# Obviously empty, must be retrieved for every session
ACCESS_TOKEN_VALUE=`cat <<END
{
    "value": "",
    "type": "text",
    "name": "accessToken",
    "key": "accessToken"
}
END`

# This is the default value for organizationId parameter
# Obviously empty, must be retrieved for every session
ORGANIZATION_ID_VALUE=`cat <<END
{
    "value": "",
    "type": "text",
    "name": "organizationId",
    "key": "organizationId"
}
END`

# This is the default value for email parameter
EMAIL_VALUE=`cat <<END
{
    "value": "john@clevermaps.io",
    "type": "text",
    "name": "email",
    "key": "email"
}
END`

mv clevermaps-postman.json clevermaps-postman.json.bak
cat clevermaps-postman.json.bak \
    | sed s/{projectId}/{{projectId}}/ \
    | sed s/{dwhClusterId}/{{dwhClusterId}}/ \
    | sed s/{accountId}/{{accountId}}/ \
    | sed s/{email}/{{email}}/ \
    | sed s/{passwordResetToken}/{{passwordResetToken}}/ \
    | sed s/{invitationHash}/{{invitationHash}}/ \
    | sed s/{organizationId}/{{organizationId}}/ \
    | sed s/{dumpId}/{{dumpId}}/ \
    | sed s/{exportId}/{{exportId}}/ \
    | sed s/{jobId}/{{jobId}}/ \
    | jq ".environments[0].values[.environments[0].values | length] |= . + $PROJECT_ID_VALUE" \
    | jq ".environments[0].values[.environments[0].values | length] |= . + $DWH_CLUSTER_ID_VALUE" \
    | jq ".environments[0].values[.environments[0].values | length] |= . + $ACCOUNT_ID_VALUE" \
    | jq ".environments[0].values[.environments[0].values | length] |= . + $ACCESS_TOKEN_VALUE" \
    | jq ".environments[0].values[.environments[0].values | length] |= . + $ORGANIZATION_ID_VALUE" \
    | jq ".environments[0].values[.environments[0].values | length] |= . + $EMAIL_VALUE" \
    > clevermaps-postman.json
