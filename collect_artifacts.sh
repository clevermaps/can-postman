#!/bin/bash

cd $BITBUCKET_CLONE_DIR

# send a request for access token, recieve json response, parse the "access_token" key and assign it to ACCESS_TOKEN
ACCESS_TOKEN_RESPONSE=$(curl -X POST -u $BITBUCKET_KEY:$BITBUCKET_SECRET https://bitbucket.org/site/oauth2/access_token -d grant_type=client_credentials)
ACCESS_TOKEN=$(echo $ACCESS_TOKEN_RESPONSE | grep access_token | awk '{ print $2 }' | sed s/\"//g | sed s/,//g)

# post the postman.json file. show only the last error and fail on non-2xx response
curl -X POST --silent --show-error --fail \
  -H "Authorization:Bearer $ACCESS_TOKEN" \
  https://api.bitbucket.org/2.0/repositories/clevermaps/can-postman/downloads/ \
  -F files=@clevermaps-postman.json
