# Postman configuration

This project converts CleverAnalytics [API blueprint](https://cleveranalytics.docs.apiary.io/) on Apiary.io 
to [Postman](https://www.getpostman.com/) JSON configuration.

## Installation

Just import [the settings](https://bitbucket.org/clevermaps/can-postman/downloads/clevermaps-postman.json) to the Postman.
